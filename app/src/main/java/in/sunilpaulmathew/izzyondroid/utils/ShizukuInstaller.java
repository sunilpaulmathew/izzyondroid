package in.sunilpaulmathew.izzyondroid.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import java.io.File;

import in.sunilpaulmathew.izzyondroid.BuildConfig;
import in.sunilpaulmathew.izzyondroid.services.UserService;
import in.sunilpaulmathew.sCommon.CommonUtils.sCommonUtils;
import rikka.shizuku.Shizuku;
import sunilpaulmathew.izzyondroid.IUserService;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on October 31, 2022
 */
public class ShizukuInstaller {

    private static IUserService mUserService = null;

    public static boolean isShizukuSupported() {
        return Shizuku.pingBinder() && Shizuku.getVersion() >= 11 && !Shizuku.isPreV11();
    }

    public static boolean ensureUserService() {
        if (mUserService != null) {
            return true;
        }

        Shizuku.UserServiceArgs mUserServiceArgs = new Shizuku.UserServiceArgs(new ComponentName(BuildConfig.APPLICATION_ID, UserService.class.getName()))
                .daemon(false)
                .processNameSuffix("service")
                .debuggable(BuildConfig.DEBUG)
                .version(BuildConfig.VERSION_CODE);
        Shizuku.bindUserService(mUserServiceArgs, mServiceConnection);

        return false;
    }

    private static final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (iBinder == null || !iBinder.pingBinder()) {
                return;
            }

            mUserService = IUserService.Stub.asInterface(iBinder);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    };

    public static String runCommand(String command) {
        if (ensureUserService()) {
            try {
                return mUserService.executeShellCommand(command);
            } catch (RemoteException ignored) {
            }
        }
        return "";
    }

    public static void installAPK(File apkFile, Context context) {
        runCommand("cp " + apkFile.getAbsolutePath() + " /data/local/tmp/APK.apk");
        sCommonUtils.saveString("installationStatus", runCommand("pm install /data/local/tmp/APK.apk"), context);
        runCommand("rm -r /data/local/tmp/APK.apk");
    }

}